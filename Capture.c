#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "BBBCAM.h"
#include <stdint.h>
#include <time.h>
#include <linux/i2c-dev.h>
#include <linux/spi/spidev.h>
#include <getopt.h>
#include <linux/types.h>
#include "ov2640_regs.h"
#include <signal.h>
int i2c2;
static char *i2cdev2 = "/dev/i2c-2";
#define OV2640_CHIPID_HIGH      0x0A
#define OV2640_CHIPID_LOW       0x0B

int ArduCAM(uint8_t model)
{

	int ret;
	myCAM.sensor_model = model;
//	printf("correct device");
	myCAM.sensor_addr = 0x30;

	//initialize spi0
	spi0 = open(spidev0, O_RDWR);
	if (spi0 < 0)
		printf("can't open device");
	ret = ioctl(spi0, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		printf("can't set spi mode");
	ret = ioctl(spi0, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		printf("can't get spi mode");
	ret = ioctl(spi0, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't set bits per word");
	ret = ioctl(spi0, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		printf("can't get bits per word");
	ret = ioctl(spi0, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't set max speed hz");
	ret = ioctl(spi0, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		printf("can't get max speed hz");

	if((i2c2 = open(i2cdev2, O_RDWR)) < 0)
	{
			perror("Failed to open i2c device.\n");
			exit(1);
	}

	if(ioctl(i2c2, I2C_SLAVE, myCAM.sensor_addr) < 0)
	{
			printf("Failed to access bus.\n");
			exit(1);
	}
}

void flush_fifo(void)
{
	write_reg(ARDUCHIP_FIFO, FIFO_CLEAR_MASK);
}

void capture(void)
{
	write_reg(ARDUCHIP_FIFO, FIFO_START_MASK);
}

void clear_fifo_flag(void)
{
	write_reg(ARDUCHIP_FIFO, FIFO_CLEAR_MASK);
}

uint8_t read_fifo(void)
{
	uint8_t data;
	data = bus_read(0x3D);
	return data;
}

uint8_t read_reg(uint8_t addr)
{
	uint8_t data;
	data = bus_read(addr & 0x7F);
	return data;
}

void write_reg(uint8_t addr, uint8_t data)
{
	bus_write(addr | 0x80, data);
}

void bus_write(uint8_t address, uint8_t value)
{
	int ret;
	uint8_t tx[2]={address,value};
	uint8_t rx[2]={0,};
	struct spi_ioc_transfer tr = {
				 .tx_buf = (unsigned long)tx,   
				 .rx_buf = (unsigned long)rx,   
				 .len = ARRAY_SIZE(tx),
				 .delay_usecs = delay,
				 .speed_hz = speed,
				 .bits_per_word = bits,
		  };
	ret = ioctl(spi0, SPI_IOC_MESSAGE(1), &tr);
}

uint8_t bus_read(uint8_t address)
{
	int ret;
	uint8_t tx[2]={address,0x00};
	uint8_t rx[2]={0,};
	struct spi_ioc_transfer tr = {
				 .tx_buf = (unsigned long)tx,   
				 .rx_buf = (unsigned long)rx,   
				 .len = ARRAY_SIZE(tx),
				 .delay_usecs = delay,
				 .speed_hz = speed,
				 .bits_per_word = bits,
		  };
	ret = ioctl(spi0, SPI_IOC_MESSAGE(1), &tr);
  	return rx[1];
}

uint8_t wrSensorReg8_8(uint8_t regID, uint8_t regDat)
{
	char wbuf[2]={regID,regDat}; //first byte is address to write. others are bytes to be written
	write(i2c2, wbuf, 2);
	return 1;
}

uint8_t rdSensorReg8_8(uint8_t regID, uint8_t* regDat)
{

	char read_start_buf[1] = {regID};
	char rbuf[1];
	write(i2c2, read_start_buf, 1); //reposition file pointer to register 0x28
	read(i2c2, rbuf, 1);
	*regDat = rbuf[0];
	return 1;
}

int wrSensorRegs8_8(const struct sensor_reg reglist[])
{
	int err = 0;
	uint16_t reg_addr = 0;
	uint16_t reg_val = 0;
	const struct sensor_reg *next = reglist;

	while ((reg_addr != 0xff) | (reg_val != 0xff))
	{
		reg_addr = pgm_read_word(&next->reg);
		reg_val = pgm_read_word(&next->val);
		err = wrSensorReg8_8(reg_addr, reg_val);
   	next++;
	}
	return 1;
}


void OV2640_set_JPEG_size(uint8_t size)
{
	switch(size)
	{
		case OV2640_160x120:
			wrSensorRegs8_8(OV2640_160x120_JPEG);
			break;
		case OV2640_176x144:
			wrSensorRegs8_8(OV2640_176x144_JPEG);
			break;
		case OV2640_320x240:
			wrSensorRegs8_8(OV2640_320x240_JPEG);
			break;
		case OV2640_352x288:
			wrSensorRegs8_8(OV2640_352x288_JPEG);
			break;
		case OV2640_640x480:
			wrSensorRegs8_8(OV2640_640x480_JPEG);
			break;
		case OV2640_800x600:
			wrSensorRegs8_8(OV2640_800x600_JPEG);
			break;
		case OV2640_1024x768:
			wrSensorRegs8_8(OV2640_1024x768_JPEG);
			break;
		case OV2640_1280x1024:
			wrSensorRegs8_8(OV2640_1280x1024_JPEG);
			break;
		case OV2640_1600x1200:
			wrSensorRegs8_8(OV2640_1600x1200_JPEG);
			break;
		default:
			wrSensorRegs8_8(OV2640_320x240_JPEG);
			break;
	}
}

void getnowtime()
{
	char sec[2];
	char min[2];
	char hour[2];
	char day[2];
	char mon[2];
	char year[4];
	time_t timep;
	struct tm *p;
	time(&timep);
	p=localtime(&timep);
	memset(nowtime,0,20);

	sprintf(year,"%d",(1900+p->tm_year));
	strcat(nowtime,year);
	if((1+p->tm_mon) < 10)
	strcat(nowtime,"0");
	sprintf(mon,"%d",(1+p->tm_mon));
	strcat(nowtime,mon);
	if(p->tm_mday < 10)
	strcat(nowtime,"0");
	sprintf(day,"%d",p->tm_mday);
	strcat(nowtime,day);
	if(p->tm_hour < 10)
	strcat(nowtime,"0");
	sprintf(hour,"%d",p->tm_hour);
	strcat(nowtime,hour);
	if(p->tm_min < 10)
	strcat(nowtime,"0");
	sprintf(min,"%d",p->tm_min);
	strcat(nowtime,min);
	if(p->tm_sec < 10)
	strcat(nowtime,"0");
	sprintf(sec,"%d",p->tm_sec);
	strcat(nowtime,sec);
	printf("nowtime is %s\n",nowtime);

}

void delayms(int i)
{
	while(i)
	{i--;}
}

void setup()
{
  uint8_t vid,pid;
  uint8_t temp;
  ArduCAM(OV2640);
  printf("ArduCAM Start!\n");

  //Check if the ArduCAM SPI bus is OK
  write_reg(ARDUCHIP_TEST1, 0x55);
  temp = read_reg(ARDUCHIP_TEST1);
  if(temp != 0x55)
  {
  	printf("SPI interface Error!\n");
  	while(1);
  }

  //Change MCU mode
  write_reg(ARDUCHIP_MODE, 0x00);
  //Check if the camera module type is OV2640
  rdSensorReg8_8(OV2640_CHIPID_HIGH, &vid);
  //printf("vid is : %x",vid);
  rdSensorReg8_8(OV2640_CHIPID_LOW, &pid);
  //printf("   pid is : %x\n",pid);
  if((vid != 0x26) || (pid != 0x42))
  	printf("Can't find OV2640 module!\n");
  else
  	printf("OV2640 detected\n");

  //Change to BMP capture mode and initialize the OV2640 module
  wrSensorReg8_8(0xff, 0x01);
  wrSensorReg8_8(0x12, 0x80);
  delayms(100);
  wrSensorRegs8_8(OV2640_JPEG_INIT);
  wrSensorRegs8_8(OV2640_YUV422);
  wrSensorRegs8_8(OV2640_JPEG);
  wrSensorReg8_8(0xff, 0x01);
  wrSensorReg8_8(0x15, 0x00);
  wrSensorRegs8_8(OV2640_1280x1024_JPEG);
  printf("Finished Register Setup!\n");

}
int counter = 1;
int main(void)
{
	int status;
	status = access("/sys/class/gpio/gpio68/value", F_OK );
        if (status == -1) {
        // file doesn't exist
                printf("GPIO_68 file doesn't exist.\n");
                exit(1);
        }
	system("echo out > /sys/class/gpio/gpio68/direction"); 

	int nmemb = 1, camera = 0;
  	system("echo 0 > /sys/class/gpio/gpio68/value");
	setup();
	system("echo 1 > /sys/class/gpio/gpio68/value");
	setup();
    	sleep(1);
  	system("echo 0 > /sys/class/gpio/gpio68/value");

	while(1)
	{
		uint8_t buf[256];
		static int i = 0;
		static int k = 0;
		static int n = 0;
		uint8_t temp,temp_last;
		uint8_t start_capture = 0;
		//Wait trigger from shutter buttom
		if(read_reg(ARDUCHIP_TRIG) & counter)
		{
			start_capture = 1;
		}
		if(start_capture)
		{
			//Flush the FIFO
			flush_fifo();
			//Clear the capture done flag
			clear_fifo_flag();
			//Start capture
			capture();
			printf("Start Capture\n");
		}

		if(read_reg(ARDUCHIP_TRIG) & CAP_DONE_MASK)
		{
			camera ++;
			printf("Capture Done!\n");
			//Construct a file name
			memset(filePath,0,20);
			if(camera == 1){
				strcat(filePath,"Pictures/left1_");
			}
			else{
				strcat(filePath,"Pictures/right1_");
				counter ++;
			}
			getnowtime();
			strcat(filePath,nowtime);
			strcat(filePath,".jpg");
			//Open the new file
			fp = fopen(filePath,"w+");
			if (fp == NULL)
			{
				printf("open file failed\n");
				return 0;

			}
			i = 0;
			temp = read_fifo();
			//Write first image data to buffer
			buf[i++] = temp;

			//Read JPEG data from FIFO
			while( (temp != 0xD9) | (temp_last != 0xFF) )
			{
				temp_last = temp;
				temp = read_fifo();
				//Write image data to buffer if not full
				if(i < 256)
					buf[i++] = temp;
				else
				{
					//Write 256 uint8_ts image data to file
					fwrite(buf,256,nmemb,fp);
					i = 0;
					buf[i++] = temp;
				}
			}
			printf("Image saving finished\n");
			//Write the remain uint8_ts in the buffer
			if(i > 0)
				fwrite(buf,i,nmemb,fp);

			//Close the file
			fclose(fp);

			//Clear the capture done flag
			clear_fifo_flag();
			//Clear the start capture flag
			start_capture = 0;
		  	system("echo 1 > /sys/class/gpio/gpio68/value");

		}

		if(counter == 2){
			exit(1);
		}
	}
}
