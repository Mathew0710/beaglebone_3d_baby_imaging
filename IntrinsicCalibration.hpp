//
//  IntrinsicCalibration.hpp
//  Calibration
//
//  Created by Mathew Strydom on 2019/08/20.
//  Copyright © 2019 Christopher Mathew Strydom. All rights reserved.
//

#ifndef IntrinsicCalibration_h
#define IntrinsicCalibration_h

#include <stdio.h>
#include <iostream>
#include <cmath>

// Vector storing the chessboard corners 2D coordinates. reset for each image
std::vector< cv::Point2f > corners;

//File directory to store images with drawn detected chessboard corners
std::string Debug_Dir = "Chessboard_Detect_Debug/";

void GaussianBlur( cv::Mat originalImage , cv::Mat &blurredImage){
    cv::Mat Gaussian(3,3, cv::DataType<double>::type);
    double factor = 1.0;
    Gaussian.at<double>(0,0) = 1;
    Gaussian.at<double>(0,1) = 2;
    Gaussian.at<double>(0,2) = 1;
    Gaussian.at<double>(1,0) = 2;
    Gaussian.at<double>(1,1) = 4;
    Gaussian.at<double>(1,2) = 2;
    Gaussian.at<double>(2,0) = 1;
    Gaussian.at<double>(2,1) = 2;
    Gaussian.at<double>(2,2) = 1;

    factor = 1.0/16.0;
	std::cout << "Image: " << originalImage << std::endl;
    std::cout << "Applying Filter" << std::endl;
    for(int x = 0; x < originalImage.cols; x++){
        for(int y = 0; y < originalImage.rows; y++){
            double grayValue = 0.0;
		std::cout << "Row: " << y << std::endl;
		std::cout << "Col: " << x << std::endl;
            for(int filterY = 0; filterY < 3; filterY++){
                for(int filterX = 0; filterX < 3; filterX++){
                    int imageX = (x - 3 / 2 + filterX + originalImage.cols) % originalImage.cols;
                    int imageY = (y - 3 / 2 + filterY + originalImage.rows) % originalImage.rows;
                    grayValue = grayValue + originalImage.at<double>(imageY, imageX)*Gaussian.at<double>(filterY,filterX);
                }
            }
            blurredImage.at<double>(y, x) = fmin(fmax(int(factor*grayValue), 0), 255);
        }
    }
//    if(count == 0){
//        cv::imwrite("../Range_Images/Left_Sharpened_W" + std::to_string(filterHeight) + ".png", sharpenedImage);
//    }
//    else{
//        cv::imwrite("../Range_Images/Right_Sharpened_W" + std::to_string(filterHeight) + ".png", sharpenedImage);
//    }
    return;
}

// Setup calibration by loading each image, finding the chessboard corners and saving the values to the relevant vector of vectors.
void setupCalibration(std::vector<std::vector<cv::Point3f >> &object_points, std::vector<std::vector<cv::Point2f >> &image_points
                      ,int board_width, int board_height, int num_imgs, double square_size, std::string side, cv::Mat &image){
    
    cv::Mat gray_temp, gray_image;
    // Image size. number of chessboard corners (internal corners only)
    cv::Size board_size = cv::Size(board_width, board_height);
    
    // Loop through each image in working directory
    for (int k = 1; k <= num_imgs; k++) {
        std::string img_file = "New_Pictures/" + side + std::to_string(k) + ".jpg";
//        std::string img_file = "../Range_Images/Left_Rectified.png";
        // read Image
        image = cv::imread(img_file);
        // Convert image to grayscale
        cv::cvtColor(image, gray_temp, CV_BGR2GRAY);
        // Varibale to check if corners have been found
	GaussianBlur(gray_temp, gray_image);

        bool found = false;
        // Finds chessboard corners and returns their 2D coordinates using adaptive thresholding
        found = cv::findChessboardCorners(image, board_size, corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
        
        // Have chessboard corners been found
        if(found == true){
            // further refines the corners detected above by searching smaller regions
            cv::cornerSubPix(gray_image, corners, cv::Size(5, 5), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
            
            // Temp variable for storing the object points
            std::vector< cv::Point3f > obj;
            
            // Iterates through the number of images and number of corners and creates a matrix spaced apart by the chessboard square size.
            for (int i = 0; i < board_height; i++){
                for (int j = 0; j < board_width; j++){
                    obj.push_back(cv::Point3f((double)j * square_size, (double)i * square_size, 0));
                }
            }
            
            // Store the corners and object points for the current image.
            printf("Found corners in %s image : %d\n", side.c_str(), k);
            image_points.push_back(corners);
            object_points.push_back(obj);
            
            // Draw and display the corners
            cv::drawChessboardCorners(gray_image, board_size, corners, found);
            cv::imwrite(Debug_Dir + side + std::to_string(k) + ".png", gray_image);
        }
    }
    return;
}

//double BilinearInterpolation(double x1, double x2, double y1, double y2, double x, double y,  double q11,double q12,double q21,double q22 )
//{
//    double x2x1, y1y2, x2x, y1y, yy2, xx1;
//    x2x1 = x2 - x1;
//    y1y2 = y1 - y2;
//    x2x = x2 - x;
//    y1y = y1 - y;
//    yy2 = y - y2;
//    xx1 = x - x1;
//    double I1 = ((x2x)/(x2x1))*q12 + ((xx1)/(x2x1))*q22;
//    //    cout << "I1: " << I1 << endl;
//    double I2 = ((x2x)/(x2x1))*q11 + ((xx1)/(x2x1))*q21;
//    //    cout << "I2: " << I2 << endl;
//    double I3 = ((y1y)/(y1y2))*I1 + ((yy2)/(y1y2))*I2;
//    //    cout << "I3: " << I3 << endl;
//    return I3;
//}
//
//void inverseDist(cv::Mat K, cv::Mat D, int num_imgs, std::string side){
//    double rd = 0.0, ru = 0.0, q = 0.0, p = 0.0, delta = 0.0, o_x, o_y, x_d, y_d;
//    cv::Mat image, gray_image, undistorted(360, 640, cv::DataType<double>::type);
////    o_x = 0.0;
////    o_y = 0.0;
//    o_x = 330.42;
//    o_y = 130.37;
////    o_x = 314.654462724923;
////    o_y = 187.4244611848112;
////    o_x = K.at<double>(0,2);
////    o_y = K.at<double>(1,2);
//
//    for(int view = 1; view <= num_imgs; view++) {
//        std::string img_file = "../Samples/" + side + std::to_string(view) + ".jpg";
//
//        image = cv::imread(img_file);
//        cv::cvtColor(image, gray_image, cv::COLOR_BGR2GRAY);
//        for(int y_u = 0; y_u < gray_image.rows; y_u ++){
//            for(int x_u = 0; x_u < gray_image.cols; x_u ++){
//                ru = sqrt(pow((x_u - o_x), 2) + pow((y_u - o_y), 2));
////                q = -ru/0.8732;
////                p = 1/0.8732;
//                q = -ru/D.at<double>(0);
//                p = 1/D.at<double>(0);
//                delta = pow(q, 2) + (4/27)*pow(p, 3);
//                rd = cbrt((-q + sqrt(delta))/2) + cbrt((-q - sqrt(delta))/2);
//                x_d = (rd/ru)*(x_u - o_x) + o_x;
//                y_d = (rd/ru)*(y_u - o_y) + o_y;
//
//                double col11 = floor(x_d);
//                double col12 = ceil(x_d);
//                double row11 = ceil(y_d);
//                double row12 = floor(y_d);
//
//                undistorted.at<double>(y_u, x_u) = BilinearInterpolation(col11, col12, row11, row12, x_d, y_d, gray_image.at<uchar>(row11,col11), gray_image.at<uchar>(row12,col11), gray_image.at<uchar>(row11,col12), gray_image.at<uchar>(row12,col12));
//
//            }
//        }
//        std::string file = "../Undistorted/" + side + std::to_string(view) + ".jpg";
//        cv::imwrite(file, undistorted);
//
//    }
//
//    return;
//}

// computes the Matrix used for normalising the object and image points
void getNormalisingMatrix(std::vector<cv::Mat> &Normalised, cv::Mat pts, int num_corners, int check){
    double sum_x = 0.0, sum_y = 0.0, mean_x = 0.0, mean_y = 0.0, var_x = 0.0, var_y = 0.0, s_x = 0.0, s_y = 0.0;
    
    cv::Mat Norm(3, 3, cv::DataType<double>::type, cv::Scalar(0));
    
    // calculate sum of the corner values which is used to find the average
    for(int i = 0; i < num_corners; i ++){
        sum_x += pts.at<double>(0, i);
        sum_y += pts.at<double>(1, i);
    }
    // Calculating the X and Y average of the image or object points
    mean_x = sum_x/num_corners;
    mean_y = sum_y/num_corners;
    // Calculate the x and y variace of the points
    for(int i = 0; i < num_corners; i++){
        var_x += pow(pts.at<double>(0, i) - mean_x, 2);
        var_y += pow(pts.at<double>(1, i) - mean_y, 2);
    }
    var_x = var_x/num_corners;
    var_y = var_y/num_corners;
    
    // calculating values used for normilasation
    s_x = sqrt(2/var_x);
    s_y = sqrt(2/var_y);
    
    //Populating the normalising matrix
    Norm.at<double>(0, 0) = s_x;
    Norm.at<double>(0, 1) = 0.0;
    Norm.at<double>(0, 2) = -s_x*mean_x;
    Norm.at<double>(1, 0) = 0.0;
    Norm.at<double>(1, 1) = s_y;
    Norm.at<double>(1, 2) = -s_y*mean_y;
    Norm.at<double>(2, 0) = 0.0;
    Norm.at<double>(2, 1) = 0.0;
    Norm.at<double>(2, 2) = 1.0;
    
    // saving the matrix to the relevant vector.
    Normalised.push_back(Norm);
    return;
}

// Normalise the image and object points.
void normalise(std::vector<cv::Mat> &Normalised_x, std::vector<cv::Mat> &Normalised_u, std::vector<std::vector<cv::Point2f>> image_points, std::vector<std::vector<cv::Point3f >> object_points, std::vector<std::vector<cv::Point3f>> &Norm_Img, std::vector<std::vector<cv::Point3f>> &Norm_Obj,
               int num_corners, int num_views){
    // Vector for storing the normalised values for a particular image.
    std::vector< cv::Point3f > Norm_Object_Element, Norm_Image_Element;
    
    // Loop through each image and normalise the points
    for(int i = 0; i < num_views; i++){

        // Extended image points saved to the temporary matrix ImPoints
        cv::Mat ImPoints(3, num_corners, cv::DataType<double>::type, cv::Scalar(0));
        for (int a = 0; a < num_corners; a++) {
            ImPoints.at<double>(0, a) = image_points[i][a].x;
            ImPoints.at<double>(1, a) = image_points[i][a].y;
            ImPoints.at<double>(2, a) = 1;
        }
        // Extended object points saved to the temporary matrix ObjPoints assuming Z = 0
        cv::Mat ObjPoints(3, num_corners, cv::DataType<double>::type, cv::Scalar(0));
        for (int b = 0; b < num_corners; b++) {
            ObjPoints.at<double>(0, b) = object_points[i][b].x;
            ObjPoints.at<double>(1, b) = object_points[i][b].y;
            ObjPoints.at<double>(2, b) = 1;
        }
        
        // Calculate Normalising Matrices
        getNormalisingMatrix(Normalised_x,ObjPoints, num_corners, 0);
        getNormalisingMatrix(Normalised_u,ImPoints, num_corners, 1);
        
        // Clear Matrix elements
        Norm_Image_Element.clear();
        Norm_Object_Element.clear();
        
        // Normalise each corner in each view
        for(int x = 0; x < num_corners; x ++){
            // Mulitplying by the Normlaising Matrix for the respective view to find the normalised points
            cv::Mat Norm_Object = Normalised_x[i]*ObjPoints.col(x);
            cv::Mat Norm_Image = Normalised_u[i]*ImPoints.col(x);
            
            // Storing the normalised values for current point
            Norm_Image_Element.push_back(cv::Point3f(Norm_Image.at<double>(0), Norm_Image.at<double>(1), Norm_Image.at<double>(2)));
            Norm_Object_Element.push_back(cv::Point3f(Norm_Object.at<double>(0), Norm_Object.at<double>(1), Norm_Object.at<double>(2)));
        }
        // Storing the normalised values for each view
        Norm_Img.push_back(Norm_Image_Element);
        Norm_Obj.push_back(Norm_Object_Element);
    }
    return;
}

int findMin(cv::Mat S){
    int index = 0;
    int size = S.rows;
    double temp = S.at<double>(0);
    
    for(int i = 0; i < size; i ++){
        if(S.at<double>(i) < temp){
            temp = S.at<double>(i);
            index = i;
        }
    }
    return index;
}


void computeHomography(std::vector<cv::Mat> &Homography, std::vector<cv::Mat> Normalised_x, std::vector<cv::Mat> Normalised_u
                       ,std::vector<std::vector<cv::Point3f>>Norm_Img
                       , std::vector<std::vector<cv::Point3f>>Norm_Obj, int index, int N){
    
    int argmin;
    cv::Mat M(2*N, 9, cv::DataType<double>::type);
    cv::Mat L(2*N, 12, cv::DataType<double>::type);
    cv::Mat U(2*N, 2*N, cv::DataType<double>::type);
    cv::Mat S(1,9, cv::DataType<double>::type);
    cv::Mat Vh(9, 9, cv::DataType<double>::type);
    cv::Mat Norm_h(3, 3, cv::DataType<double>::type);
    cv::Mat H_temp1(3, 3, cv::DataType<double>::type);
    cv::Mat H_temp2(3, 3, cv::DataType<double>::type);
    cv::Mat H(3, 3, cv::DataType<double>::type);
    double X = 0.0, Y = 0.0, Z = 0.0, u = 0.0, v = 0.0;
    
    // Itterate for every corner
    for(int i = 0; i < N; i ++){
        // extract the X,Y,Z coridinates for the image and object points
        X = Norm_Obj[index][i].x;
        Y = Norm_Obj[index][i].y;
        Z = Norm_Obj[index][i].z;
        u = Norm_Img[index][i].x;
        v = Norm_Img[index][i].y;
        
        // Set up M Matrix for even rows
        M.at<double>(2*i, 0) = -X;
        M.at<double>(2*i, 1) = -Y;
        M.at<double>(2*i, 2) = -1.0;
        M.at<double>(2*i, 3) = 0.0;
        M.at<double>(2*i, 4) = 0.0;
        M.at<double>(2*i, 5) = 0.0;
        M.at<double>(2*i, 6) = X*u;
        M.at<double>(2*i, 7) = Y*u;
        M.at<double>(2*i, 8) = u;
        
        // Set up M Matrix for odd rows
        M.at<double>(2*i+1, 0) = 0.0;
        M.at<double>(2*i+1, 1) = 0.0;
        M.at<double>(2*i+1, 2) = 0.0;
        M.at<double>(2*i+1, 3) = -X;
        M.at<double>(2*i+1, 4) = -Y;
        M.at<double>(2*i+1, 5) = -1.0;
        M.at<double>(2*i+1, 6) = X*v;
        M.at<double>(2*i+1, 7) = Y*v;
        M.at<double>(2*i+1, 8) = v;
        
    }
    // Decompose Matrix M into the left (U) and right (Vh) singular matrices along with the singular (S) values
    cv::SVD::compute(M, S, U, Vh);
    
    // Find the minimum singular value.
    argmin = findMin(S);
    
    // Non-Normalised homography matrix = to the row in Vh corresponding to the row of the min singular value in S
    H.at<double>(0, 0) = Vh.at<double>(argmin, 0);
    H.at<double>(0, 1) = Vh.at<double>(argmin, 1);
    H.at<double>(0, 2) = Vh.at<double>(argmin, 2);
    H.at<double>(1, 0) = Vh.at<double>(argmin, 3);
    H.at<double>(1, 1) = Vh.at<double>(argmin, 4);
    H.at<double>(1, 2) = Vh.at<double>(argmin, 5);
    H.at<double>(2, 0) = Vh.at<double>(argmin, 6);
    H.at<double>(2, 1) = Vh.at<double>(argmin, 7);
    H.at<double>(2, 2) = Vh.at<double>(argmin, 8);
    
    // Calculate the normalised Homography Matrix
    H_temp1 = Normalised_u[index].inv()*H*Normalised_x[index];
    Norm_h = H_temp1/H_temp1.at<double>(2,2);
    
    // Store homography for current view.
    Homography.push_back(Norm_h);
    
    return;
}

void v_pq(std::vector<cv::Mat> Homography, cv::Mat V_final, int index ){
    
    // Populating the even rows of the V matrix
    V_final.at<double>(2*index, 0) = Homography[index].at<double>(0,0) * Homography[index].at<double>(0,1);
    V_final.at<double>(2*index, 1) = (Homography[index].at<double>(0,0) * Homography[index].at<double>(1,1)) + (Homography[index].at<double>(1,0) * Homography[index].at<double>(0,1));
    V_final.at<double>(2*index, 2) = Homography[index].at<double>(1,0) * Homography[index].at<double>(1,1);
    V_final.at<double>(2*index, 3) = (Homography[index].at<double>(2,0) * Homography[index].at<double>(0,1)) + (Homography[index].at<double>(0,0) * Homography[index].at<double>(2,1));
    V_final.at<double>(2*index, 4) = (Homography[index].at<double>(2,0) * Homography[index].at<double>(1,1)) + (Homography[index].at<double>(1,0) * Homography[index].at<double>(2,0));
    V_final.at<double>(2*index, 5) = Homography[index].at<double>(2,0) * Homography[index].at<double>(2,1);
    
    // Populating the odd rows of the V matrix
    V_final.at<double>(2*index + 1, 0) = (Homography[index].at<double>(0,0) * Homography[index].at<double>(0,0)) - (Homography[index].at<double>(0,1) * Homography[index].at<double>(0,1));
    V_final.at<double>(2*index + 1, 1) = ((Homography[index].at<double>(0,0) * Homography[index].at<double>(1,0)) + (Homography[index].at<double>(1,0) * Homography[index].at<double>(0,0)))-
    ((Homography[index].at<double>(0,1) * Homography[index].at<double>(1,1)) + (Homography[index].at<double>(1,1) * Homography[index].at<double>(0,1)));
    V_final.at<double>(2*index + 1, 2) = (Homography[index].at<double>(1,0) * Homography[index].at<double>(1,0)) - (Homography[index].at<double>(1,1) * Homography[index].at<double>(1,1));
    V_final.at<double>(2*index + 1, 3) = ((Homography[index].at<double>(2,0) * Homography[index].at<double>(0,0)) + (Homography[index].at<double>(0,0) * Homography[index].at<double>(2,0)))-
    ((Homography[index].at<double>(2,1) * Homography[index].at<double>(0,1)) + (Homography[index].at<double>(0,1) * Homography[index].at<double>(2,1)));
    V_final.at<double>(2*index + 1, 4) = ((Homography[index].at<double>(2,0) * Homography[index].at<double>(1,0)) + (Homography[index].at<double>(1,0) * Homography[index].at<double>(2,0)))-
    ((Homography[index].at<double>(2,1) * Homography[index].at<double>(1,1)) + (Homography[index].at<double>(1,1) * Homography[index].at<double>(2,1)));
    V_final.at<double>(2*index + 1, 5) = (Homography[index].at<double>(2,0) * Homography[index].at<double>(2,0)) - (Homography[index].at<double>(2,1) * Homography[index].at<double>(2,1));
    return;
}

cv::Mat IntrinsicParameters(std::vector<cv::Mat> Homography, int M){
    
    double l = 0.0,vc = 0.0, alpha = 0.0, beta = 0.0, gamma = 0.0, uc = 0.0;
    cv::Mat V(2*M, 6, cv::DataType<double>::type);
    cv::Mat H(3, 3, cv::DataType<double>::type);
    cv::Mat U(2*M, 2*M, cv::DataType<double>::type);
    cv::Mat S(1, 6, cv::DataType<double>::type);
    cv::Mat Vh(6, 6, cv::DataType<double>::type);
    cv::Mat B(1, 6, cv::DataType<double>::type);
    cv::Mat K(3, 3, cv::DataType<double>::type);
    
    // Find V matrix which takes all homographies into account for intrinsic calibration
    for(int i = 0; i < M; i++){
        v_pq(Homography, V, i);
    }
    
    // Decompose Matrix V into the left (U) and right (Vh) singular matrices along with the singular (S) values
    cv::SVD::compute(V, S, U, Vh);
    // Find the minimum singular value.
    int argmin = findMin(S);
    
    // B matrix = to the row in Vh corresponding to the row of the min singular value in S
    B.at<double>(0) = Vh.at<double>(argmin, 0);
    B.at<double>(1) = Vh.at<double>(argmin, 1);
    B.at<double>(2) = Vh.at<double>(argmin, 2);
    B.at<double>(3) = Vh.at<double>(argmin, 3);
    B.at<double>(4) = Vh.at<double>(argmin, 4);
    B.at<double>(5) = Vh.at<double>(argmin, 5);
    
    // Calculate the y value of the camera centre in pixels
    vc = (( B.at<double>(0, 1)*B.at<double>(0, 3))-( B.at<double>(0, 0)* B.at<double>(0, 4)))/(( B.at<double>(0, 0)* B.at<double>(0, 2))-pow( B.at<double>(0, 1), 2));
    // Calculate a placeholder used for further calculations
    l = B.at<double>(0, 5)-(pow(B.at<double>(0, 3), 2) + vc*(B.at<double>(0, 1)*B.at<double>(0, 3)-(B.at<double>(0, 0)*B.at<double>(0, 4))))/B.at<double>(0, 0);
    // Calculate the x focal length in pixels
    alpha = sqrt(l/B.at<double>(0, 0));
    // Calculate the y focal length in pixels
    beta = sqrt((l*B.at<double>(0, 0))/((B.at<double>(0, 0)*B.at<double>(0, 2))-pow(B.at<double>(0, 1), 2)));
    // Calculate the skew of the camera in pixels
    gamma = -1.0*((B.at<double>(0, 1))*(pow(alpha, 2))*(beta/l));
    // Calculate the x value of the camera centre in pixels
    uc = (gamma*vc/beta)- (B.at<double>(0, 3)*(pow(alpha, 2))/l);
    
    // Populate the Intrinsic Matrix
    K.at<double>(0, 0) = alpha;
    K.at<double>(1, 1) = beta;
    K.at<double>(0, 1) = gamma;
    K.at<double>(0, 2) = uc;
    K.at<double>(1, 2) = vc;
    K.at<double>(2, 2) = 1.0;
    K.at<double>(1, 0) = 0.0;
    K.at<double>(2, 0) = 0.0;
    K.at<double>(2, 1) = 0.0;
    
    return K;
}


void ExtrinsicParameters(std::vector<cv::Mat> Homography, std::vector<cv::Mat> &Rotation, std::vector<cv::Mat> &Translation, cv::Mat K, int num_imgs){
    cv::Mat rvec(3, 3, cv::DataType<double>::type), tvec(3, 1, cv::DataType<double>::type), R1(1, 5, cv::DataType<double>::type, cv::Scalar(0)), R2(1, 5, cv::DataType<double>::type, cv::Scalar(0)), R3(1, 5, cv::DataType<double>::type, cv::Scalar(0)), T(1, 5, cv::DataType<double>::type, cv::Scalar(0));
    
    for(int i = 0; i < 1; i++){
        double mu1 = 1/norm(K.inv()*Homography[i].col(0));
        double mu2 = 1/norm(K.inv()*Homography[i].col(1));
        double mu3 = 1/norm(K.inv()*Homography[i].col(2));
        
        R1 = mu1*K.inv()*Homography[i].col(0);
        R2 = mu2*K.inv()*Homography[i].col(1);
        R3 = R1.cross(R2);
        T = mu3*K.inv()*Homography[i].col(2);
        
        rvec.at<double>(0,0) = R1.at<double>(0);
        rvec.at<double>(0,1) = R2.at<double>(0);
        rvec.at<double>(0,2) = R3.at<double>(0);
        rvec.at<double>(1,0) = R1.at<double>(1);
        rvec.at<double>(1,1) = R2.at<double>(1);
        rvec.at<double>(1,2) = R3.at<double>(1);
        rvec.at<double>(2,0) = R1.at<double>(2);
        rvec.at<double>(2,1) = R2.at<double>(2);
        rvec.at<double>(2,2) = R3.at<double>(2);
        
        tvec.at<double>(0,0) = T.at<double>(0);
        tvec.at<double>(1,0) = T.at<double>(1);
        tvec.at<double>(2,0) = T.at<double>(2);
        
        Rotation.push_back(rvec);
        Translation.push_back(tvec);

    }
    return;
}

void CameraCentre(cv::Mat C1, cv::Mat C2, cv::Mat K1, cv::Mat K2, cv::Mat R1, cv::Mat R2, cv::Mat T1
                     , cv::Mat T2, cv::Mat &RelR, cv::Mat &RelT, cv::Mat &Po1, cv::Mat &Po2){
    
    cv::Mat Ex1(3,4, cv::DataType<double>::type), Ex2(3,4, cv::DataType<double>::type);
    Ex1.at<double>(0,0) = R1.at<double>(0,0);
    Ex1.at<double>(0,1) = R1.at<double>(0,1);
    Ex1.at<double>(0,2) = R1.at<double>(0,2);
    Ex1.at<double>(0,3) = T1.at<double>(0);
    Ex1.at<double>(1,0) = R1.at<double>(1,0);
    Ex1.at<double>(1,1) = R1.at<double>(1,1);
    Ex1.at<double>(1,2) = R1.at<double>(1,2);
    Ex1.at<double>(1,3) = T1.at<double>(1);
    Ex1.at<double>(2,0) = R1.at<double>(2,0);
    Ex1.at<double>(2,1) = R1.at<double>(2,1);
    Ex1.at<double>(2,2) = R1.at<double>(2,2);
    Ex1.at<double>(2,3) = T1.at<double>(2);
    
    Ex2.at<double>(0,0) = R2.at<double>(0,0);
    Ex2.at<double>(0,1) = R2.at<double>(0,1);
    Ex2.at<double>(0,2) = R2.at<double>(0,2);
    Ex2.at<double>(0,3) = T2.at<double>(0);
    Ex2.at<double>(1,0) = R2.at<double>(1,0);
    Ex2.at<double>(1,1) = R2.at<double>(1,1);
    Ex2.at<double>(1,2) = R2.at<double>(1,2);
    Ex2.at<double>(1,3) = T2.at<double>(1);
    Ex2.at<double>(2,0) = R2.at<double>(2,0);
    Ex2.at<double>(2,1) = R2.at<double>(2,1);
    Ex2.at<double>(2,2) = R2.at<double>(2,2);
    Ex2.at<double>(2,3) = T2.at<double>(2);
    
    cv::Mat Qo1(3,3, cv::DataType<double>::type), Qo2(3,3, cv::DataType<double>::type);
    Po1 = K1*Ex1;
    Po2 = K2*Ex2;
    
    Qo1.at<double>(0,0) = Po1.at<double>(0,0);
    Qo1.at<double>(0,1) = Po1.at<double>(0,1);
    Qo1.at<double>(0,2) = Po1.at<double>(0,2);
    Qo1.at<double>(1,0) = Po1.at<double>(1,0);
    Qo1.at<double>(1,1) = Po1.at<double>(1,1);
    Qo1.at<double>(1,2) = Po1.at<double>(1,2);
    Qo1.at<double>(2,0) = Po1.at<double>(2,0);
    Qo1.at<double>(2,1) = Po1.at<double>(2,1);
    Qo1.at<double>(2,2) = Po1.at<double>(2,2);
    
    Qo2.at<double>(0,0) = Po2.at<double>(0,0);
    Qo2.at<double>(0,1) = Po2.at<double>(0,1);
    Qo2.at<double>(0,2) = Po2.at<double>(0,2);
    Qo2.at<double>(1,0) = Po2.at<double>(1,0);
    Qo2.at<double>(1,1) = Po2.at<double>(1,1);
    Qo2.at<double>(1,2) = Po2.at<double>(1,2);
    Qo2.at<double>(2,0) = Po2.at<double>(2,0);
    Qo2.at<double>(2,1) = Po2.at<double>(2,1);
    Qo2.at<double>(2,2) = Po2.at<double>(2,2);
    
    C1 = -(Qo1.inv())*Po1.col(3);
    C2 = -(Qo2.inv())*Po2.col(3);
    
    RelR = R2*R1;
    RelT = T2 - (RelR*T1);
    return;
}

void SaveFiles(std::string fileName,cv::Mat K1, cv::Mat K2, cv::Mat R1, cv::Mat R2, cv::Mat T1, cv::Mat T2, cv::Mat R, cv::Mat T, cv::Mat C1, cv::Mat C2, cv::Mat Po1, cv::Mat Po2, int board_width, int board_height, double square_size, int num_imgs, cv::Mat D1, cv::Mat D2){
    
    cv::FileStorage File(fileName, cv::FileStorage::WRITE);
    File << "K1" << K1;
    File << "K2" << K2;
    File << "R1" << R1;
    File << "R2" << R2;
    File << "T1" << T1;
    File << "T2" << T2;
    File << "R" << R;
    File << "T" << T;
    File << "C1" << C1;
    File << "C2" << C2;
    File << "Po1" << Po1;
    File << "Po2" << Po2;
    File << "board_width" << board_width;
    File << "board_height" << board_height;
    File << "square_size" << square_size;
    File << "num_imgs" << num_imgs;
    File << "D1" << D1;
    File << "D2" << D2;
    return;
}

#endif /* IntrinsicCalibration_h */
