//
//  main.cpp
//  Calibration
//
//  Created by Mathew Strydom on 2019/08/20.
//  Copyright © 2019 Christopher Mathew Strydom. All rights reserved.
//

#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <iostream>
#include <cmath>
#include "IntrinsicCalibration.hpp"
#include <vector>
#include <cstring>

#define board_width 9
#define board_height 6
#define num_imgs 20
#define square_size 1 //2.423 changed for Opencv

// Vector of vectors for object points (World points of the chessboard), normalised image points and normalised object points which stores 3D coordinates
std::vector< std::vector< cv::Point3f > > left_object_points, left_norm_img_pts, left_norm_obj_pts
                        ,right_object_points, right_norm_img_pts, right_norm_obj_pts;
// Vector of Vectors for image_points which stores 2D coordinates (2D cooridantes of the chessboard corners for each image)
std::vector< std::vector< cv::Point2f > > left_image_points, right_image_points;
// Normalising matrix for the object (x) and image points(u) and its inverse. Homography matrix for each image, Extrinsic paramter Matrix
std::vector < cv::Mat > left_normalised_x, left_normalised_u, left_homography, left_rotation, left_translation
                        ,right_normalised_x, right_normalised_u, right_homography, right_rotation, right_translation;

cv::Mat left_img, right_img, left_undis, right_undis;

int main(int argc, const char * argv[]) {

//    int board_width = 9, board_height = 6, num_imgs = 29;
    int num_corners = board_width*board_height;
//    double square_size = 2.432;
    
    std::string left = "left";
    std::string right = "right";
    std::string stereoYMLFile = "../Camera_files/cam_stereo.yml";
    std::string stereoTXTFile = "../Camera_files/cam_stereo.txt";
    
    cv::Mat K1(3,3, cv::DataType<double>::type), K2(3,3, cv::DataType<double>::type);
    cv::Mat C1(3,1, cv::DataType<double>::type), C2(3,1, cv::DataType<double>::type);
    cv::Mat Po1(3,4, cv::DataType<double>::type), Po2(3,4, cv::DataType<double>::type);
    cv::Mat R1, R2, T1, T2 ,R, T;

    printf("Finding Corners \n");
    setupCalibration(left_object_points, left_image_points, board_width, board_height, num_imgs, square_size, left, left_img);
    setupCalibration(right_object_points, right_image_points, board_width, board_height, num_imgs, square_size, right, right_img);
    
    cv::Mat K1temp, K2temp;
    cv::Mat D1temp(3,3, cv::DataType<double>::type, cv::Scalar(0)), D2temp(3,3, cv::DataType<double>::type, cv::Scalar(0));
    std::vector< cv::Mat > rvecs1, tvecs1, rvecs2, tvecs2;
    int flag = 0;
//    flag |= CV_CALIB_FIX_K4;
//    flag |= CV_CALIB_FIX_K5;
    
//    cv::calibrateCamera(left_object_points, left_image_points, left_img.size(), K1temp, D1temp, rvecs1, tvecs1, flag);
//    cv::calibrateCamera(right_object_points, right_image_points, right_img.size(), K2temp, D2temp, rvecs2, tvecs2, flag);
//    std::cout << K1temp << std::endl;
//    inverseDist(K1, D1temp, num_imgs, left);
//    inverseDist(K2, D2temp, num_imgs, right);
    // Normalise image and object points for each view
    printf("Starting Calibration\n");
    normalise(left_normalised_x, left_normalised_u, left_image_points, left_object_points, left_norm_img_pts, left_norm_obj_pts
              , num_corners, num_imgs);
    normalise(right_normalised_x, right_normalised_u, right_image_points, right_object_points, right_norm_img_pts, right_norm_obj_pts
              , num_corners, num_imgs);

    printf("Normilisation Complete.\n");
    printf("Number of Views Normalised:\t %d\n", num_imgs);
    printf("Number of Points per view:\t %d\n", num_corners);

    printf("Computing homography per view.\n");
    // Compute the homography matrix for each image
    for(int view = 0; view < num_imgs; view++){
        computeHomography(left_homography, left_normalised_x, left_normalised_u, left_norm_img_pts, left_norm_obj_pts
                          , view, num_corners);
        computeHomography(right_homography, right_normalised_x, right_normalised_u,right_norm_img_pts, right_norm_obj_pts
                          , view, num_corners);
    }

    printf("Computing Intrinsic Camera Parameters.\n");
    K1 = IntrinsicParameters(left_homography, num_imgs);
    K2 = IntrinsicParameters(right_homography, num_imgs);
    std::cout << "K1:\n" << K1 << std::endl;
    std::cout << "K2:\n" << K2 << std::endl;

    printf("\nComputing Extrinsic Camera Parameters.\n");
    ExtrinsicParameters(left_homography, left_rotation, left_translation, K1, num_imgs);
    ExtrinsicParameters(right_homography, right_rotation, right_translation, K2, num_imgs);
    std::cout << "R1:\n" << left_rotation[0] << std::endl;
    std::cout << "R2:\n" << right_rotation[0] << std::endl;

    R1 = left_rotation[0];
    T1 = left_translation[0];
    R2 = right_rotation[0];
    T2 = right_translation[0];
    
    printf("\nComputing Camera Centers\n");
    CameraCentre(C1, C2, K1, K2, R1, R2, T1, T2, R, T, Po1, Po2);
    
    std::cout << "C1: " << C1 << std::endl;
    std::cout << "C2: " << C2 << std::endl;
    
    SaveFiles(stereoYMLFile, K1, K2, R1, R2, T1, T2, R, T, C1, C2, Po1, Po2, board_width, board_height, square_size, num_imgs, D1temp, D2temp);
    SaveFiles(stereoTXTFile, K1, K2, R1, R2, T1, T2, R, T, C1, C2, Po1, Po2, board_width, board_height, square_size, num_imgs, D1temp, D2temp);
    
    return 0;
}
